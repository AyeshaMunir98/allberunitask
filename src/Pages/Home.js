//Libraries
import React, { useState, useEffect, useInsertionEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import {
  Box,
  Grid,
  Button
} from '@material-ui/core'

//Components
import User from '../Components/User.component'
import AddUser from '../Components/AddUser.component'
import {getData, getUsers, addUser, deleteUser} from '../Services/services'

//Style
const useStyles = makeStyles(theme => ({
  content: {
    flexGrow: 1
  },
  root: {
    display: 'flex'
  },
  submit: {
    margin: theme.spacing(0, 0, 2),
    width: '9vw',
  },
  title: {
    fontSize: 15,
    marginTop: '2%'
  },
  cancel: {
    margin: theme.spacing(1, 0, 2)
  },
  submitTrip: {
    margin: theme.spacing(5, 0, 0)
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    paddingTop:'1%',
    backgroundColor:'#f8f9fa !important',
    [theme.breakpoints.only('xs')]: {
      padding: theme.spacing(1, 2)
    },
    [theme.breakpoints.only('sm')]: {
      padding: theme.spacing(0, 1)
    },
    [theme.breakpoints.only('md')]: {
      padding: theme.spacing(1, 1)
    },
    [theme.breakpoints.only('lg')]: {
      padding: theme.spacing(1, 1)
    },
    [theme.breakpoints.only('xl')]: {
      padding: theme.spacing(0, 1)
    }
  },
  submit: {
    [theme.breakpoints.only('xs')]: {
      margin: theme.spacing(2, 0, 2)
    },
    [theme.breakpoints.only('sm')]: {
      margin: theme.spacing(2, 0, 2)
    },
    [theme.breakpoints.only('md')]: {
      margin: theme.spacing(2, 0, 2)
    },
    [theme.breakpoints.only('lg')]: {
      margin: theme.spacing(2, 0, 4)
    },
    [theme.breakpoints.only('xl')]: {
      margin: theme.spacing(2, 0, 4)
    }
  },
  root: {
    display: 'flex'
  },
  box:{
    paddingRight:'8%',
    paddingLeft:'8%'
  },
  heading: {
      marginTop:'1%',
    [theme.breakpoints.only('xs')]: {
      fontSize: 18,
      float: 'left'
    },
    [theme.breakpoints.only('sm')]: {
      fontSize: 18,
    },
    [theme.breakpoints.only('md')]: {
      fontSize: 20,
    },
    [theme.breakpoints.only('lg')]: {
      fontSize: 23,
    },
    [theme.breakpoints.only('xl')]: {
      fontSize: '1rem'
    //   float: 'left'
    }
  },
  text:{
      width: '80%'
  },
  button:{
    color: '#fff',
    textTransform: 'none',
    backgroundColor: '#0d6efd'
  }
}))


const number = [
  {
    value: 1,
    label: 'User'
  },
  {
    value: 2,
    label: 'Senior User'
  },
  {
    value: 3,
    label: 'WFM'
  }
]

export default function Home () {
  const classes = useStyles()

  const [users, setUsers] = useState([ ])
  const [roleOne, setRoleOne] = useState([])
  const [roleTwo, setRoleTwo] = useState([])
  const [roleThree, setRoleThree] = useState([])

  useEffect(() => {
    const getData = async () => {
      const usersFromServer = await getUsers().then((data) => {
        setUsers(data)
      })
    }
    getData()
  }, [])

  useEffect(() => {
    setRoleOne(users.filter((user) => user.role == number[0].value))
    setRoleTwo(users.filter((user) => user.role == number[1].value))
    setRoleThree(users.filter((user) => user.role == number[2].value))
  }, [users])

  const [cancelOpen, setCancelOpen] = useState(false) 

  const handleCancelClose = () => {
    setCancelOpen(false)
  }

  //Add User Handler
  const addNewUser = async (user) => {
    const res = await addUser(user);
    setUsers([...users, res])
  }

  //Delete User Handler
  const deleteSingle = async (userToDelete) => {
    await deleteUser(userToDelete.id);
    setUsers(users.filter((user) => user.id != userToDelete.id))
  }

  //Search Handler
  const handleSearch = (event) => {
    let text = event.target.value;
    if(text != null && text) { 
      const _filtered = users.filter((user) => user.firstName.toLowerCase().includes(text.toLowerCase()))
      setRoleOne(_filtered.filter((user) => user.role == number[0].value))
      setRoleTwo(_filtered.filter((user) => user.role == number[1].value))
      setRoleThree(_filtered.filter((user) => user.role == number[2].value))
    } else {
      setRoleOne(users.filter((user) => user.role == number[0].value))
      setRoleTwo(users.filter((user) => user.role == number[1].value))
      setRoleThree(users.filter((user) => user.role == number[2].value))
    }
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <main className={classes.content}>
        <div className={classes.toolbar} style={{backgroundColor: '#f8f9fa !important'}}>
          <Grid container>
            <Grid item xs={4} align='right'>
              <Typography className={classes.heading}>
                Search for employee:
              </Typography>
              </Grid>
                <Grid item xs={8}>
                  <TextField
                    size='small'
                    onChange={handleSearch}
                    className={classes.text}
                    variant='outlined'
                  />
                </Grid>
              </Grid>
          </div>
        <Box mt={'2%'} className={classes.box} style={{backgroundColor:'#fff'}}>
          {
            roleOne.length > 0
            ? (
            <>
              <Box pt={'4%'} />
              <Typography variant ='h5'>
                <b>Users</b>
              </Typography>
              <User 
                data={roleOne}
                role='User'
                onDeleteUser = {deleteSingle}
              />
            </>
            )
            : null
          }

          {
            roleTwo.length > 0
            ? (
            <>
              <Box mt={'4%'} />
              <Typography variant ='h5'>
              <b>Senior Users</b>
              </Typography>
              <User 
                data={roleTwo}
                role='Senior User'
                onDeleteUser = {deleteSingle}
              />
            </>
            )
            : null
          }

          {
            roleThree.length > 0
            ? (
            <>
              <Box mt={'4%'} />
              <Typography variant ='h5'><b>WFM</b></Typography>
              <User 
                data={roleThree}
                role='WFM'
                onDeleteUser = {deleteSingle}
              />
            </>
            )
            : null
          }
            <Box mt={'2%'} mb={'2%'}>
            <Button variant='outlined' className={classes.button} 
              onClick={() => setCancelOpen(true)}>
              Add User
            </Button>
          </Box>
        </Box>
      </main>

      {/*Add User Component*/}
      <AddUser
        open={cancelOpen}
        orderID={1}
        handleClose={handleCancelClose}
        onSubmit = {addNewUser}
      />
    </div>
  )
}
