//Libraries
import React, { useState } from 'react'
import {
  Box,
  Dialog,
  Typography,
  makeStyles,
  TextField,
  MenuItem,
  Button
} from '@material-ui/core'
import { Form, Formik } from 'formik'

//Components
import Yup from '../Yup/YupOValidation.schemas'

//Style
const useStyles = makeStyles(theme => ({
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular
  },
  submit: {
    margin: theme.spacing(0, 0, 2),
    width: '9vw'
  },
  title: {
    fontSize: 15,
    marginTop: '2%'
  }
}))

const number = [
    {
      value: 1,
      label: 'User'
    },
    {
      value: 2,
      label: 'Senior User'
    },
    {
      value: 3,
      label: 'WFM'
    }
  ]

export default function AddUser ({ open, orderID, handleClose, onSubmit }) {
  const classes = useStyles()
  
  const [initialValues, setInitialValues] = useState(
    {
        firstName: null,
        lastName: null,
        role: null
    }
  )
 
  return (
    <Dialog
      fullWidth={true}
      maxWidth='sm'
      open={open}
      onClose={handleClose}
      style={{ borderRadius: 8 }}
      aria-labelledby='max-width-dialog-title'
    >
      <Box p={3}>
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={Yup.AddValidationSchema}
          onSubmit={values => {
            onSubmit(values)
            handleClose()
          }}
          >
          {({ errors, touched }) => (
            <Form className={classes.form}>
              <Typography className={classes.title}>
                Enter First Name
              </Typography>
              <TextField
                error={
                  errors.firstName && touched.firstName
                }
                helperText={
                errors.firstName && touched.firstName
                  ? errors.firstName
                  : null
                }
                id='firstName'
                name='firstName'
                variant='outlined'
                className={classes.title}
                onChange={e =>
                  setInitialValues({
                    ...initialValues,
                    firstName: e.target.value
                  })
                }
                fullWidth
              />
              <Typography className={classes.title}>Enter Last Name</Typography>
              <TextField
                error={
                  errors.lastName && touched.lastName
                }
                helperText={
                  errors.lastName && touched.lastName
                    ? errors.lastName
                    : null
                }
                id='lastName'
                name='lastName'
                variant='outlined'
                className={classes.title}
                fullWidth
                onChange={e =>
                  setInitialValues({
                    ...initialValues,
                    lastName: e.target.value
                  })
                  }
                />
                <Typography className={classes.title}>Select Role</Typography>
                <TextField
                  error={errors.role && touched.role}
                  helperText={
                    errors.role && touched.role ? errors.role : null
                  }
                  fullWidth
                  variant='outlined'
                  margin='normal'
                  id='role'
                  name='role'
                  onChange={e =>
                    setInitialValues({
                      ...initialValues,
                      role: e.target.value
                    })
                  }
                  select
                  className={classes.title}
                >
                  {number.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
                <Box
                  mt={3}
                  display='flex'
                  style={{ float: 'right', marginRight: '1%' }}
                >
                  <Button
                    onClick={() => handleClose()}
                    style={{
                      backgroundColor: '#F5F6FE',
                      marginRight: '3%'
                    }}
                    size='large'
                    variant='contained'
                    className={classes.submit}
                  >
                    Cancel 
                  </Button>
                  <Button
                    color='primary'
                    size='large'
                    variant='contained'
                    type='submit'
                    className={classes.submit}
                  >
                    Submit
                  </Button>
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
    </Dialog>
  )
}