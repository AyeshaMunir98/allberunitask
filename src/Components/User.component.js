//Libraries
import React from 'react';
import { 
  makeStyles, 
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
 } from '@material-ui/core'

 //Style
const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 650,
    border: "none",
    boxShadow: "none"
  },
  btn:{
    color: '#dc3545',
    borderColor:'#dc3545',
    textTransform: 'none',
  },
  row: {
    height: "30px"
  },
  cell: {
    fontWeight: 'bold'
  }
}))

export default function User({ data, role, onDeleteUser }) {
  const classes = useStyles()

  return (
    
    <TableContainer>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow className={classes.row}>
            <TableCell className={classes.cell}>#</TableCell>
            <TableCell className={classes.cell}>First</TableCell>
            <TableCell className={classes.cell}>Last</TableCell>
            <TableCell className={classes.cell}>Role</TableCell>
            <TableCell className={classes.cell}></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, index) => (
            <TableRow key={row.name} className={classes.row}>
              <TableCell scope="row">
                {index+1}
              </TableCell>
              <TableCell align="left">
                {row.firstName}
              </TableCell>
              <TableCell align="left">{row.lastName}</TableCell>
              <TableCell align="left">{role}</TableCell>
              <TableCell align="left">
                  <Button 
                    variant='outlined' 
                    className={classes.btn} 
                    onClick={() =>  onDeleteUser(row)}>
                    Delete
                  </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
