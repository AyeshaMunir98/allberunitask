import * as yup from 'yup'

const schemata = {
  AddValidationSchema: yup.object().shape({
    firstName: yup
      .string()
      .required('This field is required')
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ")
      ,
    lastName: yup
      .string()
      .required('This field is required')
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ")
      ,
    role: yup
      .number()
      .required('This field is required.')
      .nullable()
  })
}

export default schemata
