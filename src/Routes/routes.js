import { Routes, Route, Navigate } from 'react-router-dom'
import Home from '../Pages/Home'

const AppRoutes = () => {
  let routes
    routes = (
      <Routes>
        <Route exact path='/' element={<Home />} />
      </Routes>
    )

  return routes
}
export default AppRoutes
