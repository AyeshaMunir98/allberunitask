import backendPath from '../Utils/constants'

// Fetch users from data
  const getUsers = async () => {
    const res = await fetch(backendPath);
    const usersData = await res.json();
    return usersData;
  }


//Add User
  const addUser = async (user) => {
    const res = await fetch(backendPath, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json'
      },
      body: JSON.stringify(user)
    })
    const data = await res.json()
    return data;
  }

  //Delete User
  const deleteUser = async (id) => {
    await fetch(`${backendPath}/${id}`, {
      method: 'DELETE'
    })
    return id;
  }
  

  export  { getUsers, addUser, deleteUser }
