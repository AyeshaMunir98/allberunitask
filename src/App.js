import Home from "./Pages/Home";
import React, { Component }  from 'react';
import Routes from './Routes/routes'

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
